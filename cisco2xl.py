#Code gets into devices and extracts specific details and writes into XLSX doc.
#Each column has a fuction of it's own.

from netmiko import ConnectHandler
from openpyxl import load_workbook
from xxx import *

try:
    wb = load_workbook("files/cmdb.xlsx")

except FileNotFoundError:
    print("Error opening the generic_XL file. Check the file path")

s = wb["ss"]

def rtModel(nC,r,s,hn):#IOS Version
    if nC == "UNREACHABLE":
        v = "UNREACHABLE"

    else:
        try:
            o = nC.send_command("show inventory | inc PID")
            l1 = o.split("\n")
            v = l1[0].split()[1]
            #print(hn ,":",v)
  
    s.cell(row=r,column=1).value = hn
    s.cell(row=r,column=2).value = v

def main():
    u = input("Username:")
    p = getpass("Password:")
    
    mL = open("hosts","r").read().rstrip().split("\n")
    for i in mL:
        x  = s.max_row #Get the max value of the row to move to the next
        r  = x + 1 #Get the next row
        ip = i.split()[0]
        hn = i.split()[1]
        nC = ssh(ip,u,p)
        rtModel(nC,r,s,hn)
        save()
        print(hn, ": Details written on the spreadsheet\n")
        
def ssh(ip,u,p):
    
    dev1 = {'device_type':"cisco_ios",'ip': ip,'username':u,'password': p}

    try:
        nC = ConnectHandler(**dev1)

    except:
        nC = "UNREACHABLE"
        
    return nC

def save(): #Function for saving the xlsx document
    try:
        wb.save("files/cmdb.xlsx")
        wb.close()

    except PermissionError:
        print("No saving done!!")

main()
