OVERVIEW

Script logs into cisco devices and extract information needed and writes it to a .xlsx file.
Functions can be edited to obtain the specific information needed.

HOW TO USE
-Ensure that you have a .xlsx file named cmdb.xlsx in a files/ directory.
-Ensure that you have a "hosts" file with the ips and hostnames detailed.

Example :
cat > hosts
10.2.2.2 router01
10.3.3.3 switch12

NB:
-You might need to install the Netmiko module.
-You might need to install the openpyxl module.

